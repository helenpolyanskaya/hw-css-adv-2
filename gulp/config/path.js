import * as nodePath from 'path';
const rootFolder =  nodePath.basename(nodePath.resolve());

const buildFolder = `./dist`;
const srcFolder = `./src`;
const all = `./`;

export const path = {
    build: {
        html: `${buildFolder}/`,
        files: `${buildFolder}/`,

    },
    src: {
        html: `${srcFolder}/*.html`,
        scss: `${srcFolder}/scss/**/*.*`,
        js: `${srcFolder}/js/**/*.*`,
        img: `${srcFolder}/img/**/*.*`,
    },
    watch: {
        html: `${all}/**/*.html`,
        files: `${srcFolder}/**/*.*`,
        img: `${srcFolder}/img/**/*.*`,
        js: `${srcFolder}/js/**/*.*`,
    },
    clean: buildFolder,
       buildFolder: buildFolder,
    srcFolder: srcFolder,
    rootFolder: rootFolder,
    ftp: ``
}
