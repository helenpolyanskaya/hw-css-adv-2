import gulp from "gulp";
import dartSass from 'sass';
import gulpSass from 'gulp-sass';
const sass = gulpSass(dartSass);
import concat from 'gulp-concat';
import cleanCSS from 'gulp-clean-css';
import autoprefixer from 'gulp-autoprefixer';
import minifyjs from 'gulp-js-minify';
import imagemin from 'gulp-imagemin';
import browserSync from 'browser-sync';
const brwsrSnc = browserSync.create();

export const browserSyncInit = () => {
    return   brwsrSnc.init({
        server: {
            baseDir: './'
        },
    })
}
export const browserSyncReload = () => {
  brwsrSnc.reload();
  return Promise.resolve('reloaded');
}

export const copyCss = () => {
    return app.gulp.src(app.path.src.scss)
      .pipe(concat('styles.scss'))
      .pipe(sass().on('error', sass.logError))
      .pipe(autoprefixer())
      .pipe(cleanCSS())
      .pipe(app.gulp.dest(app.path.build.files))
}

export const copyJs = () => {
    return app.gulp.src(app.path.src.js)
      .pipe(minifyjs())
      .pipe(app.gulp.dest(app.path.build.files))
      .pipe(browserSync.reload({
          stream: true
      }))
}

export const copyImg = () => {
    return app.gulp.src(app.path.src.img)
    //   .pipe(imagemin())
      .pipe(app.gulp.dest(app.path.build.files))
      .pipe(browserSync.reload({
          stream: true
      }))
}