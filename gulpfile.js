import gulp from "gulp";
import { path } from "./gulp/config/path.js";
global.app = {
    path: path,
    gulp: gulp,
}

import { copyJs, copyCss, copyImg, browserSyncInit, browserSyncReload } from "./gulp/task/copy.js";
import { reset } from "./gulp/task/reset.js";


function watcher() {
  browserSyncInit();
  gulp.watch(app.path.src.js, copyJs)
  gulp.watch(app.path.src.scss, gulp.series(copyCss, browserSyncReload))
  // gulp.watch(app.path.src.scss, copyCss)
  gulp.watch(app.path.src.img, copyImg)
}

const dev = gulp.series(reset, gulp.parallel(copyJs, copyImg, copyCss), watcher);

// Сценарій: 
gulp.task('default', dev);
gulp.task('dev', dev);
gulp.task('build', gulp.series(reset, gulp.parallel(copyJs, copyImg, copyCss)));
