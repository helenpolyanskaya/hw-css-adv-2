let menuButton = document.querySelector('.menu_button');
let menuButtonClick = document.querySelector('.menu_button_click');
let navMenu = document.querySelector('.nav_menu');
let screen = window.matchMedia( '(min-width: 1199px)' );


window.onresize = function() {
    if (screen.matches) {
        navMenu.style = "";
        menuButton.style = "";
        menuButtonClick.style = "";
    }
}

menuButton.onclick = function() {
    menuButton.style.display = "none";
    menuButtonClick.style.display = "flex";
    navMenu.style.display = "flex";
}

menuButtonClick.onclick = function() {
    
    menuButtonClick.style.display = "none";
    menuButton.style.display = "flex";
    navMenu.style.display = "none";
}

